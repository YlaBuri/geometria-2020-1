Narrativa:
Como um usuario na funcionalidade de calculo de triangulos
desejo informar a tamanho dos catetos
de modo que possa receber o valor da hipotenusa

Cenario: Calculo da hipotenusa
Dado que estou na funcionalidade de calculo de triangulos
Quando seleciono o tipo de calculo Hipotenusa
E informo 3 para cateto1
E informo 4 para cateto2
E solicito que o calculo seja realizado
Ent�o a hipotenusa calculada sera 5

