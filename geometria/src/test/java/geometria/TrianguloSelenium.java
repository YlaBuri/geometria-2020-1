package geometria;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TrianguloSelenium {
	public static WebDriver driver;

	@Test
	public void entrar() {
		System.setProperty("webdriver.chrome.driver", "C:/Users/yla_m/Downloads/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("C:/Users/yla_m/git/geometria-2020-1/geometria/src/main/webapp/triangulo.html");
	}
	
	@Test
	public void selecionarHipotenusa() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Hipotenusa");

	}
	
	@Test
	public void selecionarCateto() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Cateto");

	}

	@Test
	public void informarCateto1(String cateto1) {
		WebElement inputCateto1 = driver.findElement(By.id("cateto1"));
		inputCateto1.sendKeys(cateto1);


	}

	@Test
	public void informarCateto2(String cateto2)  {
		WebElement inputCateto2 = driver.findElement(By.id("cateto2"));	
		inputCateto2.sendKeys(cateto2);

	}
	
	@Test
	public void informarHipotenusa(String hipotenusa) {
		WebElement inputHipotenusa = driver.findElement(By.id("hipotenusa"));
		inputHipotenusa.sendKeys(hipotenusa);
	}

	@Test
	public void calcular() {
		WebElement calcularBtn = driver.findElement(By.id("calcularBtn"));
		
		calcularBtn.click();
	}
	
	@Test
	public String obterHipotenusa() {
		
		WebElement valor = driver.findElement(By.id("hipotenusa"));
		return valor.getText();
	}

	@Test
	public String obterCateto() {
		WebElement valor = driver.findElement(By.id("cateto2"));
		return valor.getText();
	}
	
	@AfterAll
	public static void teardown() {
		driver.quit();
	}
}
