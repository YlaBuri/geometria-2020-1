package geometria;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class CalculoHipotenusaSteps {

	TrianguloSelenium triangulo = new TrianguloSelenium();


	@Given("estou na funcionalidade de calculo de triangulos")
	public void inicar() {
		triangulo.entrar();
	}

	@When("seleciono o tipo de calculo Hipotenusa")
	public void escolherHipotenusa() {
		triangulo.selecionarHipotenusa();
	}


	@When("informo $cateto para cateto1")
	public void informarCateto1(String cateto) {
		triangulo.informarCateto1(cateto);
	}


	@When("informo $cateto2 para cateto2")
	public void informarNota(String cateto2) {
		triangulo.informarCateto2(cateto2);
	}

	@When("solicito que o calculo seja realizado")
	public void calcular() {
		triangulo.calcular();
	}


	@Then("a hipotenusa calculada sera $hipotenusa")
	public void verificarValor(String hipotenusa) {
		String h=triangulo.obterHipotenusa();
		System.out.println("OBTIDO: "+h);
		Assert.assertEquals( hipotenusa, h);
	}

}
