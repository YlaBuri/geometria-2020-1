package geometria;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class CalculoCatetoSteps {
	
	TrianguloSelenium triangulo = new TrianguloSelenium();
	
	@Given("estou na funcionalidade de calculo de triangulos")
	public void inicar() {
        triangulo.entrar();
	}

	
	@When("seleciono o tipo de calculo Cateto")
	public void escolherCateto() {
        triangulo.selecionarCateto();;
	}

	@When("informo $cateto para cateto1")
	public void informarCateto1(String cateto) {
		triangulo.informarCateto1(cateto);
	}

	@When("informo $hipotenusa para hipotenusa")
	public void informarHipotenusa(String hipotenusa) {
		triangulo.informarHipotenusa(hipotenusa);;
	}
	
	@When("solicito que o calculo seja realizado")
	public void calcular() {
		triangulo.calcular();
	}

	@Then("o cateto2 calculado sera $cateto2")
	public void verificarValor(String cateto2) {
		Assert.assertEquals(cateto2, triangulo.obterHipotenusa());
	}

}
